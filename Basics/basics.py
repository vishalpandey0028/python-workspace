# import datetime # importimg package/module datetime

# print current date time using print function
# print("date time is ", datetime.datetime.now())

# Varaiables Examples---
# mynuber = 10
# mytext = "text"
# myfloat = 12.13
# myboolean = False

# print function also takes multiple values . Please check other arguments for more use of print function----
# print("my text", mytext, "mynumber", mynuber)

# python doesn't contains type declaration
# type function can be used to get the type of any varaibles
# simple data type int , str, float

# print(type(mynuber), type(mytext), type(myfloat)) # Expected Output <class 'int'> <class 'str'> <class 'float'>

# compound data type containing simple data or compound types type Ex:- List
# sampleList = [1, "1", True, 1.12]
# print(sampleList)  # Expected Output: [1, '1', True, 1.12]

# Range function to generate list of sequential numbers
# print(list(range(1,10,2))); # range(start,end,step)

# dir function to get info all methods or objects present in specified object
# print(dir(list))
# print(dir(str))
# print(dir(__builtins__)) # to get info of all builtin available type and functions in python

# help function to get info about any object or functions
# print(help(str.upper))

# string funstions upper - returns uppercase of any string/ title - capitalize any string / lower , etc..

# sum functions can be applied on compound types
# print(sum([1,2,34,5.3])) # is applicable for list containing int or float

# max function to get max value out of any list

# count function of list type returns the  no of occurrences of specified value
# print([1,2,3,4,22,2,4].count(4)) #Expected Output: 2

# Compound type dist contains list of key:value items enclosed with {} Ex: {"a":1,"b":2}
# .keys() to get keys list ["a"."b"]
# .values() to get values list [1,2] 